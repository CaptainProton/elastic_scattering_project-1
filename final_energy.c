#include <math.h>

//---------------------------
//NEWTON ROOT FINDER 
//---------------------------
double newton(double A, double thetaL){
    double f, fprime, error, thetaC_new, thetaC, tan_thetaL;
    int iter=0;

    tan_thetaL = tan(thetaL);
    thetaC = M_PI/2;//initial guess of thetaC

    do{
        f = sin(thetaC) - tan_thetaL/A - tan_thetaL*cos(thetaC);
        fprime = cos(thetaC) + tan_thetaL*sin(thetaC);

        thetaC_new = thetaC - f/fprime;
        error = fabs((thetaC_new - thetaC)/thetaC);
        thetaC = thetaC_new;

        iter++;
        }while((error>1e-6) && (iter<50));

    return thetaC;
    }


//---------------------------
//CALCULATE FINAL ENERGY AFTER COLLISION 
//---------------------------
double final_energy(const double Ei,const double A,const double thetaL){
    double beta, alpha, Ef,thetaC;
  
    beta = (A-1)/(A+1);
    alpha = pow(beta,2); 
    thetaC = newton(A, thetaL);
  
    Ef = 0.5 * Ei * ((1+alpha) + (1-alpha)*cos(thetaC));

    return Ef;  
    }

