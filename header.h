#ifndef HEADER_H
#define HEADER_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* MACRO to get a random number in X in [0-1] */
#define RN ((double)rand()/(RAND_MAX))

/* Particle structure definition */
struct Particle{
  double X;     /* X position */
};

/* /\* Material structure definition *\/ */
/* struct Material{ */
/*   double Width;     /\* Width of the material          *\/ */
/*   double Path;      /\* Mean free path of the material *\/ */
/*   double A;         /\* Mass number                    *\/ */
/* }; */


/* function prototypes */
double * updater(const int, const float, const double, const double, const double, const double);
double displacement(const double, const double);
double angle(const double);
double final_energy(const double, const double, const double);

#endif /* HEADER_H */
